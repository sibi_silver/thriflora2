import { Component, OnInit, Input, Output,EventEmitter, SimpleChanges } from '@angular/core';

@Component({
selector: 'app-button',
templateUrl: './button.component.html',
styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
clickEvent() {
throw new Error('Method not implemented.');
}
    @Input() enabled?: string
    @Output() messageEvent:  EventEmitter<string> = new EventEmitter<string>();
    
    disabled:boolean = true;
msgEvent: any;

constructor() {}

ngOnChanges(changes: SimpleChanges) {
    if(changes.enabled.currentValue === 'true') {
        this.disabled =false
    } else {
        this.disabled = true
    }
}
ngOnInit(): void {
}

onClick(): void {
    this.messageEvent.emit
    
}
}

