import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss']
})
export class TextboxComponent implements OnInit {
  @Output() textboxContentEvent = new EventEmitter<string>()
    
  placeholder: string = 'Introduzca su e-mail'
  textboxContent: string = ''
  state: string = ''
  
  ngOnInit(): void {
  }
  validateEmail(event: boolean) {
    const regularExpression = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
    if(regularExpression.test(this.textboxContent)) {
      this.state= 'right'
    }
    else{
      this.state= 'wrong'
    }
  }
}
