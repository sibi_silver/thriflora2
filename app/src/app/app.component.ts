import { Component, OnInit,  } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: string = 'Thriflora';
  messageText?: string;
  validateEmail: boolean = false;
  checkboxEvent: boolean = false;
  enabledButton: string = ''

  constructor(private service: AppService) {}

  ngOnInit() {
  }
  getTheChecked(event: boolean) {
    this.checkboxEvent = event
    this.getEnabledButton()
  }
  getEmail(event: boolean) {
    this.validateEmail = event
    this.getEnabledButton()
  }
  getEnabledButton(){
    const emailValidation = this.validateEmail
    const checkboxValidation = this.checkboxEvent
    if (emailValidation === true && checkboxValidation === true){
      this.enabledButton = 'true'
    } else {
      this.enabledButton = 'false'
    }
  }
  message(event: string): void {
    this.messageText = event
  }

}
